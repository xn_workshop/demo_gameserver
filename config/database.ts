import * as dotenv from 'dotenv';

dotenv.config();

const databases = {
    mysql: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        database: process.env.DB_DATABASE,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        charset: 'utf8mb4',
        collation: 'utf8mb4_unicode_ci',
        prefix: '',
        prefix_indexes: true,
        strict: true,
        engine: null,
        options: []
    },
    pgsql : {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        database: process.env.DB_DATABASE,
        username: process.env.DB_USERNAME,
        password: process.env.DB_PASSWORD,
        charset: 'utf8',
        prefix: '',
        prefix_indexes: true,
        schema: 'public',
        sslmode: 'prefer',
        options: []
    },
    redis: {
        options: {
            cluster: process.env.REDIS_CLUSTER,
            prefix: '',
        },
        default: {
            host: process.env.REDIS_HOST,
            password: process.env.REDIS_PASSWORD,
            port: process.env.REDIS_PORT,
            database: process.env.REDIS_DATABASE,
        },
        cache: {
            host: process.env.REDIS_HOST,
            password: process.env.REDIS_PASSWORD,
            port: process.env.REDIS_PORT,
            database: process.env.REDIS_CACHE_DATABASE,
        },
    },
    redis_sentinel: {
        default: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            options: {
                service: process.env.REDIS_SENTINEL_SERVICE,
                parameters: {
                    password: process.env.REDIS_PASSWORD,
                    database: process.env.REDIS_DATABASE,
                },
            },
        },
        cache: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            options: {
                service: process.env.REDIS_SENTINEL_SERVICE,
                parameters: {
                    password: process.env.REDIS_PASSWORD,
                    database: process.env.REDIS_CACHE_DATABASE,
                },
            },
        },
    },
}

export {
    databases
};