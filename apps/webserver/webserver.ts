import logger from "../../utils/logger";
const express = require("express");
const path = require("path");

if (process.argv.length < 3) {
    console.log("node webserver.js port");
}

const app = express();
const port = parseInt((process.argv[2]));
app.use(express.static(path.join(process.cwd(), "/apps/webserver", "www_root")));
app.listen(port);

logger.log('info', `webserver starting on ${port}`);