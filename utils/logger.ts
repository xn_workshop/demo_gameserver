import {Logger} from "winston";

const winston = require('winston');
const fs = require( 'fs' );
const path = require('path');
const logDir = 'output/';

if ( !fs.existsSync( logDir ) ) {
    // Create the directory if it does not exist
    fs.mkdirSync( logDir );
}

const logger: Logger = winston.createLogger({
    // 當 transport 不指定 level 時 , 使用 info 等級
    level: 'info',
    // 設定輸出格式
    format: winston.format.json(),
    // 設定此 logger 的日誌輸出器
    transports: [
        // 只有 error 等級的錯誤 , 才會將訊息寫到 error.log 檔案中
        new winston.transports.File({ filename: path.join(process.cwd(), logDir, 'error.log'), level: 'error' }),
        // info or 以上的等級的訊息 , 將訊息寫入 combined.log 檔案中
        new winston.transports.File({ filename: path.join(process.cwd(), logDir, 'combined.log') }),
    ],
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        level: 'info',
        handleExceptions: true,
        humanReadableUnhandledException: true,
        json: false,
        colorize: { all: true },
        stderrLevels: ['error', 'alert', 'critical'],
    }));
}

export {
    logger
}