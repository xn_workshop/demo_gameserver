import { logger } from "../utils/logger";
import { ProtoMgr, ProtoType } from "./protomgr";

class ServiceMgr {

    /**
     * 全部的服務
     * @private
     */
    private static services = {};

    /**
     * 註冊服務
     * @param stype
     * @param service
     */
    public static register_service(stype, service) {
        if (this.services[stype] ) {
            logger.warn(`${this.services[stype].name} service is registered.`)
        }
        this.services[stype] = service;
        service.init();
    }

    /**
     * 客戶端掉線
     * @param session
     */
    public static on_client_lost_connection(session) {
        logger.warn(`session lost: ${session.session_key}`);
        for (const key in this.services) {
            this.services[key].on_player_disconnect(session);
        }
    }

    /**
     * 客戶端操作
     * @param session
     * @param str_or_buf
     */
    public static on_recv_client_cmd(session, str_or_buf) {
        let cmd = ProtoMgr.decode_cmd(session.proto_type, str_or_buf);
        if (!cmd) {
            return false;
        }
        let stype, ctype, body;
        stype = cmd[0];
        ctype = cmd[1];
        body = cmd[2];

        logger.info(`${stype} ${ctype} ${body}`);

        if (this.services[stype]) {
            this.services[stype].on_recv_player_cmd(session, ctype, body);
        }

        return true;
    }
}

export {
    ServiceMgr
}