import * as net from "net";
import * as ws from "ws";
import { logger } from "../utils/logger";
import { TCPPkg } from "./tcppkg";
import { ProtoMgr, ProtoType } from "./protomgr";
import { ServiceMgr } from "./servicemgr";

class NetBus {

    private global_session_list = {};

    private global_session_key = 1;

    /**
     * 客戶端建立連線
     * @param session
     * @param proto_type
     * @param is_ws
     */
    private on_session_enter(session, proto_type, is_ws) {
        if (is_ws) {
            logger.info(`session enter ${session._socket.remoteAddress} ${session._socket.remotePort}`);
        }
        else {
            logger.info(`session enter ${session.remoteAddress} ${session.remotePort}`);
        }

        session.last_pkg = null; // 表示我们存储的上一次没有处理完的TCP包;
        session.is_ws = is_ws;
        session.proto_type = proto_type;

        // 加入到我们的 session 列表里面
        this.global_session_list[this.global_session_key] = session;
        session.session_key = this.global_session_key;
        this.global_session_key ++;
        // end
    }

    /**
     * 客戶端離線
     * 清理session
     * @param session
     */
    private on_session_exit(session) {
        logger.info("session exit !!!!");
        ServiceMgr.on_client_lost_connection(session);
        session.last_pkg = null;
        if (this.global_session_list[session.session_key]) {
            this.global_session_list[session.session_key] = null;
            delete this.global_session_list[session.session_key]; // 把这个key, value从 {}里面删除
            session.session_key = null;
        }
    }

    /**
     * 接受客戶端操作
     * 一定能够保证是一个整包;
     * 如果是json协议 str_or_buf json字符串;
     * 如果是buf协议 str_or_buf Buffer对象;
     * @param session
     * @param str_or_buf
     */
    private on_session_recv_cmd(session, str_or_buf) {
        if (!ServiceMgr.on_recv_client_cmd(session, str_or_buf)) {
            this.session_close(session);
        }
    }

    /**
     * 发送命令
     * @param session
     * @param cmd
     */
    public session_send(session, cmd) {
        if (!session.is_ws) { //
            let data = TCPPkg.package_data(cmd);
            session.write(data);
            return;
        }
        else {
            session.send(cmd);
        }
    }

    /**
     * 關閉客戶端連線
     * @param session
     */
    public session_close(session) {
        if (!session.is_ws) {
            session.end();
            return;
        }
        else {
            session.close();
        }
    }


    /**
     * TCP客戶端事件
     * @param session
     * @param proto_type
     */
    private add_client_session_event(session, proto_type) {
        session.on("close", ()=>{
            this.on_session_exit(session);
        });

        session.on("data", (data)=>{
            //
            if (!Buffer.isBuffer(data)) { // 不合法的数据
                this.session_close(session);
                return;
            }
            // end

            let last_pkg = session.last_pkg as Buffer;
            if (last_pkg != null) { // 上一次剩余没有处理完的半包;
                last_pkg = Buffer.concat([last_pkg, data], last_pkg.length + data.length);
            }
            else {
                last_pkg = data;
            }

            let offset = 0;
            let pkg_len = TCPPkg.read_pkg_size(last_pkg, offset);
            if (pkg_len < 0) {
                return;
            }

            while(offset + pkg_len <= last_pkg.length) { // 判断是否有完整的包;
                // 根据长度信息来读取我们的数据,架设我们穿过来的是文本数据
                let cmd_buf;

                // 收到了一个完整的数据包
                if (session.proto_type == ProtoType.PROTO_JSON) {
                    let json_str = last_pkg.toString("utf8", offset + 2, offset + pkg_len);
                    if (!json_str) {
                        this.session_close(session);
                        return;
                    }
                    this.on_session_recv_cmd(session, json_str);
                }
                else {
                    cmd_buf = Buffer.allocUnsafe(pkg_len - 2); // 2个长度信息
                    last_pkg.copy(cmd_buf, 0, offset + 2, offset + pkg_len);
                    this.on_session_recv_cmd(session, cmd_buf);
                }


                offset += pkg_len;
                if (offset >= last_pkg.length) { // 正好我们的包处理完了;
                    break;
                }

                pkg_len = TCPPkg.read_pkg_size(last_pkg, offset);
                if (pkg_len < 0) {
                    break;
                }
            }

            // 能处理的数据包已经处理完成了,保存 0.几个包的数据
            if (offset >= last_pkg.length) {
                last_pkg = null;
            }
            else { // offset, length这段数据拷贝到新的Buffer里面
                let buf = Buffer.allocUnsafe(last_pkg.length - offset);
                last_pkg.copy(buf, 0, offset, last_pkg.length);
                last_pkg = buf;
            }

            session.last_pkg = last_pkg;
        });

        session.on("error", function(err) {

        });

        this.on_session_enter(session, proto_type, false);
    }

    /**
     * TCP服務啟動
     * @param ip
     * @param port
     * @param proto_type
     */
    public start_tcp_server(ip, port, proto_type) {
        logger.info(`start tcp server ${ip}:${port} ${ProtoType[proto_type]}`);
        const server = net.createServer((client_sock)=>{
            this.add_client_session_event(client_sock, proto_type);
        });

        // 监听发生错误的时候调用
        server.on("error", ()=>{
            logger.error("server listen error");
        });

        server.on("close", ()=>{
            logger.error("server listen close");
        });

        server.listen({
            port: port,
            host: ip,
            exclusive: true,
        });
    }

// ------------------------- WebSocket
    private isString(obj){ //判断对象是否是字符串
        return Object.prototype.toString.call(obj) === "[object String]";
    }

    /**
     * WS客戶端事件
     * @param session
     * @param proto_type
     */
    private ws_add_client_session_event(session, proto_type) {
        // close事件
        session.on("close", ()=>{
            this.on_session_exit(session);
        });
        // error事件
        session.on("error", (err)=>{
        });
        // end
        session.on("message", (data, isBinary)=>{
            const message = isBinary ? data : data.toString();
            if (session.proto_type === ProtoType.PROTO_JSON) {
                if (!this.isString(message)) {
                    this.session_close(session);
                    return;
                }
                this.on_session_recv_cmd(session, message);
            }
            else {
                if (!Buffer.isBuffer(message)) {
                    this.session_close(session);
                    return;
                }
                this.on_session_recv_cmd(session, message);
            }
        });
        // end
        this.on_session_enter(session, proto_type, true);
    }

    /**
     * WS服務啟動
     * @param ip
     * @param port
     * @param proto_type
     */
    public start_ws_server(ip, port, proto_type) {
        logger.info(`start ws server ${ip}:${port} ${ProtoType[proto_type]}`);
        const server = new ws.Server({
            host: ip,
            port: port,
        });

        server.on("connection", (client_sock)=>{
            this.ws_add_client_session_event(client_sock, proto_type);
        });

        server.on("error", (err)=>{
            logger.error("ws server listen error!!");
        });

        server.on("close", (err)=>{
            logger.error("ws server listen close!!");
        });
    }
}

export {
    NetBus
};
