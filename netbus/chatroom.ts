import {logger} from "../utils/logger";


class ChatRoom {

    private stype = 1;

    private name = "chat room";

    public init() {
        logger.info(`${this.name} service init`);
    }

    public on_recv_player_cmd(session, ctype, body) {
        logger.info(`${this.name} on_recv_player_cmd ${ctype} ${body}`);
    }

    public on_player_disconnect(session) {
        logger.info(`${this.name} on_player_disconnect ${session.session_key}`);
    }
}

export {
    ChatRoom
}