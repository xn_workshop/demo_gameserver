/**
 * 规定:
 * (1)服务号 命令号 不能为0
 * (2)服务号与命令号大小不能超过2个字节的整数;
 * (3) buf协议里面2个字节来存放服务号(0开始的2个字节)，命令号(1开始的2个字节);
 * (4) 加密,解密,
 * (5) 服务号命令号二进制中都用小尾存储
 * (6) 所有的文本，都使用utf8
 */

import { logger } from "../utils/logger";

enum ProtoType {
    PROTO_JSON = 1,
    PROTO_BUF
}

class ProtoMgr {

    // buf协议的编码/解码管理  stype, ctype --> encoder/decoder
    private static decoders = {}; // 保存当前我们buf协议所有的解码函数, stype,ctype --> decoder;

    private static encoders = {}; // 保存当前我们buf协议所有的编码函数, stype, ctype --> encoder

    /**
     * 加密
     * @param str_of_buf
     */
    private static encrypt_cmd(str_of_buf): string {
        return str_of_buf;
    }

    /**
     * 解密
     * @param str_of_buf
     */
    private static decrypt_cmd(str_of_buf): string {
        return str_of_buf;
    }

    /**
     * 指令加密
     * @param stype
     * @param ctype
     * @param body
     * @private
     */
    private static json_encode(stype, ctype, body) {
        let cmd = {};
        cmd[0] = stype;
        cmd[1] = ctype;
        cmd[2] = body;

        return JSON.stringify(cmd);
    }

    /**
     * 指令解密
     * @param cmd_json
     * @private
     */
    private static json_decode(cmd_json) {
        try {
            let cmd = JSON.parse(cmd_json);
            if (!cmd ||
                typeof(cmd[0])=="undefined" ||
                typeof(cmd[1])=="undefined" ||
                typeof(cmd[2])=="undefined") {
                return null;
            }
            return cmd;
        } catch (e) {
        }
        return false;
    }

    /**
     * key, value, stype + ctype -->key: value
     * @param stype
     * @param ctype
     * @private
     */
    private static get_key(stype, ctype) {
        return (stype * 65536 + ctype);
    }

    /**
     * 参数1: 协议类型 json, buf协议;
     * 参数2: 服务类型
     * 参数3: 命令号;
     * 参数4: 发送的数据本地，js对象/js文本，...
     * 返回是一段编码后的数据;
     * @param proto_type
     * @param stype
     * @param ctype
     * @param body
     */
    public static encode_cmd(proto_type, stype, ctype, body) {
        let buf = null;
        if (proto_type == ProtoType.PROTO_JSON) {
            buf = this.json_encode(stype, ctype, body);
        }
        else { // buf协议
            let key = this.get_key(stype, ctype);
            if (!this.encoders[key]) {
                return null;
            }
            // end
            buf = this.encoders[key](body);
        }

        if (buf) {
            buf = this.encrypt_cmd(buf); // 加密
        }
        return buf;
    }

    /**
     * 参数1: 协议类型
     * 参数2: 接手到的数据命令
     * 返回: {0: stype, 1, ctype, 2: body}
     * @param proto_type
     * @param str_or_buf
     */
    public static decode_cmd(proto_type, str_or_buf) {
        str_or_buf = this.decrypt_cmd(str_or_buf); // 解密

        if (proto_type == ProtoType.PROTO_JSON) {
            return this.json_decode(str_or_buf);
        }

        if (str_or_buf.length < 4) {
            return null;
        }

        let cmd = null;
        let stype = str_or_buf.readUInt16LE(0);
        let ctype = str_or_buf.readUInt16LE(2);
        let key = this.get_key(stype, ctype);

        if (!this.decoders[key]) {
            return null;
        }

        cmd = this.decoders[key](str_or_buf);
        return cmd;
    }

    /**
     * encode_func(body) return 二进制buffer对象
     * @param stype
     * @param ctype
     * @param encode_func
     */
    public static reg_buf_encoder(stype, ctype, encode_func) {
        let key = this.get_key(stype, ctype);
        if (this.encoders[key]) { // 已经注册过了，是否搞错了
            // log.warn(;
            logger.warn("stype: " + stype + " ctype: " + ctype + "is reged!!!");
        }

        this.encoders[key] = encode_func;
    }

    /**
     * decode_func(cmd_buf) return cmd { 0: 服务号, 1: 命令号, 2: body};
     * @param stype
     * @param ctype
     * @param decode_func
     */
    public static reg_buf_decoder(stype, ctype, decode_func) {
        let key = this.get_key(stype, ctype);
        if (this.decoders[key]) { // 已经注册过了，是否搞错了
            logger.warn("stype: " + stype + " ctype: " + ctype + "is reged!!!");
        }

        this.decoders[key] = decode_func;
    }
}

export {
    ProtoMgr,
    ProtoType
};
