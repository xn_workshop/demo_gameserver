class TCPPkg {

	// 根据封包协议我们读取包体的长度;
	public static read_pkg_size(pkg_data, offset) {
		if (offset > pkg_data.length - 2) { // 没有办法获取长度信息的;
			return -1;
		}
		return pkg_data.readUInt16LE(offset);
	}

	public static package_data(data) {
		let buf = Buffer.allocUnsafe(2 + data.length);
		buf.writeUInt16LE(2 + data.length, 0);
		buf.fill(data, 2);
		return buf;
	}
}

export {
	TCPPkg
};