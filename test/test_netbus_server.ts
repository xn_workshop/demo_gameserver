import { NetBus } from "../netbus/netbus";
import { ProtoMgr, ProtoType } from "../netbus/protomgr";
import { ServiceMgr } from "../netbus/servicemgr";
import { ChatRoom } from "../netbus/chatroom";

const _ = require("dotenv").config();
const netbus = new NetBus()
netbus.start_tcp_server("127.0.0.1", process.env.TCP_PORT, ProtoType.PROTO_BUF);
//
netbus.start_ws_server("127.0.0.1", process.env.WS_PORT, ProtoType.PROTO_BUF);

ServiceMgr.register_service(1, new ChatRoom());

