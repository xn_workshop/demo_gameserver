import * as net from "net";
import tcppkg from "./netpkg";

var sock = net.connect({
    port: 6080,
    host: "127.0.0.1",
}, function() {
    console.log('connected to server!');
});

sock.on("connect",function() {
    console.log("connect success");

    // sock.write(tcppkg.package_data("Hello!"));
    // sock.write(netpkg.test_pkg_two_action("start", "stop"));
    let buf_set = tcppkg.test_pkg_two_slice("Bla", "ke");

    sock.write(buf_set[0]); //
    setTimeout(function() {
        sock.write(buf_set[1]);
    }, 5000);

});