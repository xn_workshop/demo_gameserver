import * as net from "net";
import { TCPPkg } from "../netbus/tcppkg";
import { ProtoMgr, ProtoType } from "../netbus/protomgr";

const sock = net.connect({
    port: 6002,
    host: "127.0.0.1",
}, function() {
    console.log('connected to server!');
});

sock.on("connect",function() {
    console.log("connect success");

    let cmdBuf = ProtoMgr.encode_cmd(ProtoType.PROTO_JSON, 1, 2, "Hi~~~~~~~~~~~~");
    cmdBuf = TCPPkg.package_data(cmdBuf);
    sock.write(cmdBuf);
});