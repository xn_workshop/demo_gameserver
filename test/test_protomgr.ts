import {logger} from "../utils/logger";
import {NetBus} from "../netbus/netbus";
import {ProtoMgr} from "../netbus/protomgr";

let buf = ProtoMgr.encode_cmd(ProtoMgr.PROTO_JSON, 1, 1, "Hello, Xuan~~~~~~");
console.log(buf, buf.length);

let cmd = ProtoMgr.decode_cmd(ProtoMgr.PROTO_JSON, buf);
console.log(cmd);

ProtoMgr.reg_buf_encoder(1,1, () => {

});

ProtoMgr.reg_buf_decoder(1,2, () => {

});